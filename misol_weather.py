import time
import ubinascii

import logging
log = logging.getLogger(__name__)

class MisolWeatherStation(object):
    def __init__(self, rs485, timeout=None, debug=False):
        self.rs485 = rs485
        if timeout is None:
            self.timeout = 30
        else:
            self.timeout = timeout

    def init_buffer(self):
        buf = bytearray(17)
        mv = memoryview(buf)
        buf[0] = 36
        return buf, mv

    def schema(self, content):
        data_raw = dict(
            id={
                'operation': None,
                'content': content[0:2]
            },
            code={
                'operation': None,
                'content': content[2:4],
            },
            winddir={
                'operation': None,
                'content': content[4:6],
            },
            temperature={
                'operation': lambda x: (x - 400) / 10,
                'content': content[7:10],
            },
            humidity={
                'operation': None,
                'content': content[10:12],
            },
            windspeed={
                'operation': lambda x: (x / 8) * 1.12,
                'content': content[12:14],
            },
            gustspeed={
                'operation': lambda x: x * 1.12,
                'content': content[14:16],
            },
            rainfall={
                'operation': None,
                'content': content[16:20],
            },
            uv={
                'operation': None,
                'content': content[20:24],
            },
            lux={
                'operation': lambda x: x / 10,
                'content': content[24:30],
            }
        )
        return data_raw

    def load(self, schema_entry):
        payload = int(schema_entry['content'], 16)
        if schema_entry['operation'] is None:
            return payload
        else:
            return float(schema_entry['operation'](payload))

    def read_buffer(self):

        buf, mv = self.init_buffer()
        idx = 1
        log.info('Reading Weather data into RS485 buffer \n')
        now = time.time()

        while idx < len(buf) and time.time() - now < self.timeout:
            if self.rs485.any() and bytearray(self.rs485.read(1)) == bytearray(b'\x24'):
                bytes_read = self.rs485.readinto(mv[idx:])
                log.info('Got {} bytes of data'.format(bytes_read), ubinascii.hexlify(buf[0:idx + bytes_read]))
                idx += bytes_read

        checksum = b'%02X' % (sum(buf[:-1]) & 0xFF)
        checksum_read = b'%02X' % (buf[-1] & 0xFF)
        return checksum, checksum_read, buf

    def read(self):
        count = 0
        while count < 2:
            checksum, checksum_read, buf = self.read_buffer()

            # Verify checksum
            if checksum == checksum_read:
                schema = self.schema(ubinascii.hexlify(buf))
                data = {key: self.load(schema[key]) for key in schema}

                return data
            count = count + 1

        log.info('Reading failed')
        return None
