from machine import UART
from machine import Pin

from misol_weather import MisolWeatherStation


TX = "P3"
RX  = "P4"

rs485 = UART(1, baudrate=9600, pins=(RX, TX))
station = MisolWeatherStation(rs485)

while True:
    print(station.read())

